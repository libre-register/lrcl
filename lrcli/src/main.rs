#![warn(clippy::pedantic)]
#![allow(
    clippy::module_name_repetitions,
    clippy::wildcard_imports,
    clippy::used_underscore_binding,
    clippy::doc_markdown,
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    clippy::too_many_lines
)]

use std::error::Error;

use console::style;
use dialoguer::{Input, Password};
use lrcl::Client;
mod shell;

#[macro_use]
extern crate shellfish;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Greet the user
    println!("{}", style("Welcome to LibreRegister's CLI").cyan().bold());

    // Ask for the server
    let server_url: String = Input::new()
        .with_prompt("Enter the server's url")
        .interact()?;

    // Attempt a blocking connection to the server.
    //
    // I think it doesn't really make sense to connect asynchronously
    // for the server may be invalid.
    println!("{}", style("Trying to connect in...").green());
    let mut client = Client::new(&server_url).await?;

    // Get their credentials
    let username: String =
        Input::new().with_prompt("Enter your username").interact()?;
    let password: String = Password::new()
        .with_prompt("Please enter your password")
        .interact()?;

    // Try and log in
    println!("{}", style("Trying to log in...").green());
    client.login(username.trim(), password.trim()).await?;
    println!("{}", style("Log in successful!").green());

    // Start a shell.
    //
    // By the way its a safe clone as a Client uses an Arc internally.
    println!(
        "{}",
        style(
            "Starting interactive shell.\n\nType help for a list of commands."
        )
        .green()
    );
    shell::shell(Client::clone(&client)).await?;

    // Log out
    println!("{}", style("Trying to logout...").green());
    client.logout().await?;
    println!("{}", style("Logout successful!").green());

    Ok(())
}
