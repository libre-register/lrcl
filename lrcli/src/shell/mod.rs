//! # Shell
//!
//! This is for the interactive shell component, which uses shellfish.

use std::error::Error as StdError;
use std::fmt;

use console::style;
use lrcl::Client;
use shellfish::Command;
use shellfish::Shell;

pub mod contact;
pub mod login;

#[derive(Debug)]
struct StringError<'a>(&'a str);
impl fmt::Display for StringError<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}
impl StdError for StringError<'_> {}

/// This starts a shell.
pub async fn shell(client: Client) -> Result<(), Box<dyn StdError>> {
    // Defines a shell
    let mut shell = Shell::new_async(
        client,
        format!("[{}]-$ ", style("Libre Register").green().bold(),),
    );

    shell.commands.insert(
        "get_permissions".to_string(),
        Command::new_async(
            "gets our, or another user's, permissions.".to_string(),
            async_fn!(Client, login::get_permissions),
        )
        .await,
    );

    shell.commands.insert(
        "set_permissions".to_string(),
        Command::new_async(
            "sets a user's permissions.".to_string(),
            async_fn!(Client, login::permissions),
        )
        .await,
    );

    shell.commands.insert(
        "create_user".to_string(),
        Command::new_async(
            "creates a user.".to_string(),
            async_fn!(Client, login::create_user),
        )
        .await,
    );

    shell.commands.insert(
        "passwd".to_string(),
        Command::new_async(
            "resets a user's password.".to_string(),
            async_fn!(Client, login::reset_password),
        )
        .await,
    );

    shell.commands.insert(
        "create_contact".to_string(),
        Command::new_async(
            "creates a contact.".to_string(),
            async_fn!(Client, contact::create_contact),
        )
        .await,
    );

    shell.commands.insert(
        "list_contacts".to_string(),
        Command::new_async(
            "lists the contacts.".to_string(),
            async_fn!(Client, contact::list_contacts),
        )
        .await,
    );

    shell.commands.insert(
        "list_logins".to_string(),
        Command::new_async(
            "lists the users/logins.".to_string(),
            async_fn!(Client, login::list_logins),
        )
        .await,
    );

    shell.commands.insert(
        "get_contact".to_string(),
        Command::new_async(
            "gets a contact.".to_string(),
            async_fn!(Client, contact::get_contact),
        )
        .await,
    );

    shell.commands.insert(
        "update_contact".to_string(),
        Command::new_async(
            "updates a contact.".to_string(),
            async_fn!(Client, contact::update_contact),
        )
        .await,
    );

    shell.commands.insert(
        "delete_contact".to_string(),
        Command::new_async(
            "deletes a contact.".to_string(),
            async_fn!(Client, contact::delete_contact),
        )
        .await,
    );

    shell.commands.insert(
        "delete_login".to_string(),
        Command::new_async(
            "deletes a login.".to_string(),
            async_fn!(Client, login::delete_login),
        )
        .await,
    );

    shell.commands.insert(
        "link_login".to_string(),
        Command::new_async(
            "links a login so it can be used to get other resources."
                .to_string(),
            async_fn!(Client, login::link_login),
        )
        .await,
    );

    shell.commands.insert(
        "get_login_uid".to_string(),
        Command::new_async(
            "gets the uid for each login entered.".to_string(),
            async_fn!(Client, login::get_login_uid),
        )
        .await,
    );

    // Runs the shell
    shell.run_async().await?;

    Ok(())
}
