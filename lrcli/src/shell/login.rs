use std::error::Error;
use std::error::Error as StdError;
use std::fmt::Write;
use std::mem;

use console::style;
use dialoguer::{Editor, Password};
use lrcl::Client;
use structopt::StructOpt;

use super::StringError;

/// Gets our, or another user's, permissions.
pub async fn get_permissions(
    client: &mut Client,
    mut args: Vec<String>,
) -> Result<(), Box<dyn StdError>> {
    let pager = minus::Pager::new().unwrap().finish();

    // Configure the pager
    let mut guard = pager.lock().await;
    guard.set_prompt("LibreRegister");
    guard.set_exit_strategy(minus::ExitStrategy::PagerQuit);

    match &mut args.get_mut(1) {
        None => {
            // Print everything to the pager
            writeln!(
                guard,
                "{}",
                serde_json::to_string_pretty(
                    &client.get_our_permissions().await?
                )?
            )?;
            drop(guard);

            // Show the pager
            minus::tokio_updating(pager).await?;
            Ok(())
        }
        Some(x) => {
            // Print everything to the pager
            writeln!(
                guard,
                "{}",
                serde_json::to_string_pretty(
                    &client.get_permissions_by_username(x).await.map_err(
                        |e| -> Box<dyn StdError> {
                            match e {
                                lrcl::Error::InvalidStatusCodeInResponse(
                                    404,
                                ) => Box::new(StringError("Unkown object.")),
                                _ => Box::new(e),
                            }
                        }
                    )?
                )?
            )?;
            drop(guard);

            // Show the pager
            minus::tokio_updating(pager).await?;
            Ok(())
        }
    }
}

pub async fn create_user(
    client: &mut Client,
    _args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let mut user = r#"
{
    "username": "my_user_123",
    "password": "1234",
    "permissions": {
        "permissions": [
            {
                "path": ["contacts", "N"],
                "auth": true,
                "mut": false
            }
        ]
    }
}
"#
    .to_string();
    loop {
        user = Editor::new()
            .extension(".json")
            .edit(&user)
            .unwrap()
            .unwrap_or_default();

        // Validates the user, if it cannot it will reloop, thereby ensuring
        // we enter valid json.
        let mut user: lrau::User = match serde_json::from_str(&user) {
            Ok(user) => user,
            Err(e) => {
                println!("{}{}", style("ERROR:").red(), style(e).red());
                tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                continue;
            }
        };

        // Converts the plaintext password to argon2id
        let pass = mem::take(&mut user.password);
        user.reset_pass(&pass)?;

        client.create_user(&user).await?;
        break Ok(());
    }
}

pub async fn reset_password(
    client: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // Get the user from the first argument,
    let user = args.get(1).ok_or("No user specified.")?;

    // Get the password from stdin.
    let password = Password::new()
        .with_prompt(format!(
            "Enter the new password for {}",
            style(user).red().bold()
        ))
        .interact()?;

    // Confirm password
    if password != Password::new().with_prompt("Confirm password").interact()? {
        return Err(Box::new(StringError("Passwords don't match")));
    }

    client.user_reset_password(user, password).await?;

    Ok(())
}

pub async fn permissions(
    client: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // Get the user from the first argument,
    let user = args.get(1).ok_or("No user specified.")?;

    // Get their permissions
    let permissions = client.get_permissions_by_username(user).await?;
    let mut permissions = serde_json::to_string_pretty(&permissions)?;

    loop {
        permissions = Editor::new()
            .extension(".json")
            .edit(&permissions)
            .unwrap()
            .unwrap_or_default();

        // Validates the permissions, if it cannot it will reloop,
        // thereby ensuring we enter valid json.
        let permissions: lrau::Permissions =
            match serde_json::from_str(&permissions) {
                Ok(user) => user,
                Err(e) => {
                    println!("{}{}", style("ERROR:").red(), style(e).red());
                    tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                    continue;
                }
            };

        client.user_reset_permissions(user, &permissions).await?;
        break;
    }

    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "list_logins")]
pub struct LoginLister {
    /// The offset of where we shall list contacts from, for example only
    /// listing the contacts from 3 onwards or suchlike.
    #[structopt(short, long, default_value = "0")]
    pub start: usize,

    /// The amount of contacts that should be shown.
    #[structopt(short, long, default_value = "10")]
    pub count: usize,

    /// Wether or not the list will be sorted descending.
    #[structopt(short = "d", long)]
    pub sort_dec: bool,

    /// Don't include the name
    #[structopt(short = "u", long)]
    pub dont_include_uid: bool,
}

pub async fn list_logins(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // Get the options from the command line
    let opt = LoginLister::from_iter_safe(&args)?;

    // Create a pger
    let pager = minus::Pager::new().unwrap().finish();

    // Configure the pager
    let mut guard = pager.lock().await;
    guard.set_prompt("LibreRegister");
    guard.set_exit_strategy(minus::ExitStrategy::PagerQuit);
    guard.set_line_numbers(minus::LineNumbers::Enabled);

    // Prints the header
    writeln!(
        guard,
        "{}                                | {}",
        style("Username").bold(),
        style("Uid").bold(),
    )?;

    // Grab the list of uiser.
    let users = state
        .list_logins(
            opt.start..(opt.count + opt.start),
            !opt.sort_dec,
            !opt.dont_include_uid,
        )
        .await?;

    for user in users {
        writeln!(
            guard,
            "{:<40}| {}",
            user.username,
            user.uid.unwrap_or_else(|| style("None").red().to_string())
        )?;
    }
    drop(guard);

    minus::tokio_updating(pager).await?;

    Ok(())
}

pub async fn delete_login(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    match args.get(1) {
        Some(contact) => Ok(state.delete_login(contact).await?),
        None => return Err(Box::new(StringError("Please specify a login"))),
    }
}

pub async fn link_login(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    match (args.get(1), args.get(2)) {
        (Some(login), Some(uid)) => {
            Ok(state.user_set_uid(login, uid.clone()).await?)
        }
        (Some(_login), None) => {
            return Err(Box::new(StringError(
                "Please specify a uid to link to",
            )))
        }
        _ => {
            return Err(Box::new(StringError(
                "Usage: link_login [LOGIN] [UID]",
            )))
        }
    }
}

pub async fn get_login_uid(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // We will get the uids of everything we input
    for arg in &args[1..] {
        println!(
            "{}: {}",
            arg,
            state
                .get_login_uid(arg)
                .await?
                .unwrap_or_else(|| style("None").red().to_string())
        );
    }

    Ok(())
}
