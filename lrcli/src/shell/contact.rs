use std::convert::TryInto;
use std::error::Error;
use std::fmt::Write;

use console::style;
use contack::read_write::vcard::VCard;
use contack::Contact;
use dialoguer::Editor;
use lrcl::Client;
use structopt::StructOpt;

use super::StringError;

pub async fn create_contact(
    client: &mut Client,
    _args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let mut contact =
        "BEGIN:VCARD\nFN:John Doe\nN:Doe;John;;Mr;\nEND:VCARD".to_string();
    loop {
        contact = Editor::new()
            .extension(".vcard")
            .edit(&contact)
            .unwrap()
            .unwrap_or_default();

        // Validates the user, if it cannot it will reloop, thereby ensuring
        // we enter valid json.
        let vcard: contack::read_write::vcard::VCard = match contact
            .trim()
            .replace("\r", "")
            .replace("\n", "\r\n")
            .parse()
        {
            Ok(contact) => contact,
            Err(e) => {
                println!("{}{}", style("ERROR: ").red(), style(e).red());
                tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                continue;
            }
        };

        // Convert it to a contact so that we can generate a UID.
        let contact: contack::Contact = match vcard.try_into() {
            Ok(contact) => contact,
            Err(e) => {
                println!("{}{}", style("ERROR: ").red(), style(e).red());
                tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                continue;
            }
        };

        let vcard: contack::read_write::vcard::VCard = contact.into();

        client.create_contact(&vcard).await?;
        break Ok(());
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "list_contacts")]
pub struct ContactLister {
    /// The offset of where we shall list contacts from, for example only
    /// listing the contacts from 3 onwards or suchlike.
    #[structopt(short, long, default_value = "0")]
    pub start: usize,

    /// The amount of contacts that should be shown.
    #[structopt(short, long, default_value = "10")]
    pub count: usize,

    /// Wether or not the list will be sorted descending.
    #[structopt(short = "d", long)]
    pub sort_dec: bool,

    /// Don't include the name
    #[structopt(short = "n", long)]
    pub dont_include_name: bool,
}

pub async fn list_contacts(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // Get the options from the command line
    let opt = ContactLister::from_iter_safe(&args)?;

    // Create a pger
    let pager = minus::Pager::new().unwrap().finish();

    // Configure the pager
    let mut guard = pager.lock().await;
    guard.set_prompt("LibreRegister");
    guard.set_exit_strategy(minus::ExitStrategy::PagerQuit);
    guard.set_line_numbers(minus::LineNumbers::Enabled);

    // Prints the header
    writeln!(
        guard,
        "{}                                     | {}",
        style("Uid").bold(),
        style("Name").bold(),
    )?;

    // Grab the list of contacts.
    let contacts = state
        .list_contacts(
            opt.start..(opt.count + opt.start),
            !opt.sort_dec,
            !opt.dont_include_name,
        )
        .await?;

    // And display it
    for contact in contacts {
        writeln!(
            guard,
            "{:<40}| {}",
            contact.uid,
            contact.name.unwrap_or_default()
        )?;
    }

    drop(guard);

    minus::tokio_updating(pager).await?;
    Ok(())
}

pub async fn get_contact(
    client: &mut Client,
    mut args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    let pager = minus::Pager::new().unwrap().finish();

    // Configure the pager
    let mut guard = pager.lock().await;
    guard.set_prompt("LibreRegister");
    guard.set_exit_strategy(minus::ExitStrategy::PagerQuit);

    match &mut args.get_mut(1) {
        Some(contact) => {
            writeln!(
                guard,
                "{}",
                VCard::from(client.get_contact(contact).await?),
            )?;
            drop(guard);

            // Show the pager
            minus::tokio_updating(pager).await?;
            Ok(())
        }
        None => Err(Box::new(StringError(
            "Resource not specified. Please specify a contact.",
        ))),
    }
}

pub async fn update_contact(
    client: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    // Get the contact from the first argument
    let contact = args.get(1).ok_or("No contact specified.")?;

    // Get the contact
    let contact: VCard = client.get_contact(contact).await?.into();
    let mut contact: String = contact.to_string();

    loop {
        contact = Editor::new()
            .extension(".vcard")
            .edit(&contact)
            .unwrap()
            .unwrap_or_default();

        // Validates the VCard
        let contact: VCard = match contact.parse() {
            Ok(contact) => contact,
            Err(e) => {
                println!("{}{}", style("ERROR:").red(), style(e).red());
                tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                continue;
            }
        };

        // Validates the contact
        let contact: Contact = match contact.try_into() {
            Ok(c) => c,
            Err(e) => {
                println!("{}{}", style("ERROR:").red(), style(e).red());
                tokio::time::sleep(std::time::Duration::from_secs(3)).await;
                continue;
            }
        };

        let uid = contact.uid.clone();
        let vcard: VCard = contact.into();

        client.update_contact(&vcard, &uid).await?;
        break;
    }

    Ok(())
}

pub async fn delete_contact(
    state: &mut Client,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    match args.get(1) {
        Some(contact) => Ok(state.delete_contact(contact).await?),
        None => return Err(Box::new(StringError("Please specify a contact"))),
    }
}
