//! # Login
//!
//! This has functions to do with logging in and out.
use reqwest::StatusCode;

use crate::error::Error;

#[derive(Deserialize)]
pub struct ListStruct {
    pub username: String,
    pub uid: Option<String>,
}

impl super::Client {
    /// Attempts to log a user in.
    ///
    /// Once logged in, if the function is sucsessful, then the session field
    /// of the client will be activated, meaning that functions that require
    /// a login shall work.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the login
    ///    information is incorrect.
    pub async fn login(
        &mut self,
        username: &str,
        password: &str,
    ) -> Result<(), Error> {
        // Modify the url to point to the login. `/login/login
        let url = self.url.join("login/").map_err(Error::Url)?;
        let url = url.join("login").map_err(Error::Url)?;

        // Create a hashmap of our login details
        let map = hash_map!(
            "username" => username,
            "password" => password,
        );

        // Send the request to login.
        let login = self
            .client
            .post(url)
            .json(&map)
            .send()
            .await
            .map_err(Error::Networking)?;

        // See what it has sent back
        match login.status() {
            // Our login has succeeded
            StatusCode::CREATED => {
                self.session =
                    Some(login.text().await.map_err(Error::Networking)?);
                Ok(())
            }
            // Our login has failed
            StatusCode::UNAUTHORIZED => Err(Error::LoginInvalidSession),
            // Server Error / Unhandled
            _ => {
                Err(Error::InvalidStatusCodeInResponse(login.status().as_u16()))
            }
        }
    }

    /// Gets the permission's of the currently logged in user.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    pub async fn get_our_permissions(
        &mut self,
    ) -> Result<lrau::Permissions, Error> {
        if let Some(session) = &self.session {
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&[
                    "login",
                    "users",
                    "by_session",
                    session,
                    "permissions",
                ]);

            self.get_permissions(url).await
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Gets the permission's of any user. Requires the permissions
    /// `/login/permissions`
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - we
    ///    don't have the permissions for the required action.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    pub async fn get_permissions_by_username(
        &mut self,
        username: &str,
    ) -> Result<lrau::Permissions, Error> {
        if let Some(session) = &self.session {
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&[
                    "login",
                    "users",
                    "by_username",
                    username,
                    "permissions",
                ]);

            url.set_query(Some(&format!("session={}", session)));

            self.get_permissions(url).await
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    pub async fn get_permissions(
        &mut self,
        url: reqwest::Url,
    ) -> Result<lrau::Permissions, Error> {
        // Send the request to get the permissions.
        let response = self
            .client
            .get(url)
            .send()
            .await
            .map_err(Error::Networking)?;
        let status = response.status();

        // See what is has sent back
        match status {
            // Success
            StatusCode::OK => {
                let text = response.text().await.map_err(Error::Networking)?;
                Ok(serde_json::from_str(&text).map_err(Error::SerdeJson)?)
            }
            StatusCode::UNAUTHORIZED => match response
                .text()
                .await
                .map_err(Error::Networking)?
                .as_str()
            {
                "INVALID_PERMISSIONS" => Err(Error::LoginInvalidPermissions),
                "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                _ => Err(Error::InvalidStatusCodeInResponse(status.as_u16())),
            },
            _ => Err(Error::InvalidStatusCodeInResponse(status.as_u16())),
        }
    }

    /// Lets a user create another user.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    ///  * [`ResourceAlreadyCreated`](Error::ResourceAlreadyCreated) - the
    ///    resource has already been created, and we shall not duplicate it.
    pub async fn create_user(
        &mut self,
        user: &lrau::User,
    ) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point to the url `/login/user/?<session>`
            let mut url = self
                .url
                .join("login/")
                .and_then(|x| x.join("users"))
                .map_err(Error::Url)?;

            url.set_query(Some(&format!("session={}", session)));
            // Send the request to create a user.
            let response = self
                .client
                .post(url)
                .json(user)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what is has sent back
            match status {
                // Success
                StatusCode::CREATED => Ok(()),
                StatusCode::CONFLICT => Err(Error::ResourceAlreadyCreated),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Lets a user reset the password of another user.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - cannot find the
    ///  resource to modify.
    pub async fn user_reset_password(
        &self,
        username: &str,
        password: String,
    ) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point to the url
            // `/login/users/<username>/password?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "users", username, "password"]);

            url.set_query(Some(&format!("session={}", session)));

            // Send the request to reset the password.
            let response = self
                .client
                .patch(url)
                .body(password)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what is has sent back
            match status {
                // Success
                StatusCode::NO_CONTENT => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Lets a user set the uid of another user. Note that this action can
    /// only be done once, so once you have set a uid that is it - it has
    /// been set.
    ///
    /// You require the permission `login/uid`
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - cannot find the
    ///  resource to modify.
    pub async fn user_set_uid(
        &self,
        username: &str,
        uid: String,
    ) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point to the url
            // `/login/users/<username>/uid?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "users", username, "uid"]);

            url.query_pairs_mut().append_pair("session", session);

            // Send the request to reset the password.
            let response = self
                .client
                .post(url)
                .body(uid)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what is has sent back
            match status {
                // Success
                StatusCode::CREATED => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::CONFLICT => Err(Error::ResourceAlreadyCreated),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Lets a user reset another user's permissions. Note it would be best
    /// not to reset but rather modify by getting the data before pushing it.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - cannot find the
    ///  resource to modify.
    pub async fn user_reset_permissions(
        &self,
        username: &str,
        permissions: &lrau::Permissions,
    ) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point to the url
            // `/login/users/<username>/permissions?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "users", username, "permissions"]);

            url.set_query(Some(&format!("session={}", session)));

            // Send the request to reset the permissions.
            let response = self
                .client
                .patch(url)
                .json(permissions)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what is has sent back
            match status {
                // Success
                StatusCode::NO_CONTENT => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Lets a user log out and clear the session information.
    ///
    /// # Errors
    ///
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    pub async fn logout(&mut self) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point to the url `/login/logout/<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "logout", session]);

            // Send the request to log out.
            let response = self
                .client
                .delete(url)
                .send()
                .await
                .map_err(Error::Networking)?;

            // See what is has sent back
            match response.status() {
                // Success
                StatusCode::NO_CONTENT => {
                    self.session = None;
                    Ok(())
                }
                StatusCode::UNAUTHORIZED => Err(Error::LoginInvalidSession),
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Generates a list of usernames from the server. Requires the user
    /// to have the `/login/username` permission
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    pub async fn list_logins(
        &mut self,
        range: std::ops::Range<usize>,
        sort_asc: bool,
        include_uid: bool,
    ) -> Result<Vec<ListStruct>, Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point at /login/list
            let mut url: reqwest::Url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "list"]);

            let params = [
                ("session", session),
                ("sort_asc", &sort_asc.to_string()),
                ("offset", &range.start.to_string()),
                ("limit", &(range.end - range.start).to_string()),
                ("include_uid", &include_uid.to_string()),
            ];

            // Send the request to list the logins
            let response = self
                .client
                .get(url)
                .form(&params)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();
            match status {
                StatusCode::OK => {
                    let text =
                        response.text().await.map_err(Error::Networking)?;
                    serde_json::from_str(&text).map_err(Error::SerdeJson)
                }
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Deletes a login.
    ///
    /// You need the following permissions:
    ///
    ///  * `/login/permissions`
    ///  * `/login/username`
    ///  * `/login/password`
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - the resource cannot
    ///    be found.
    pub async fn delete_login(&self, username: &str) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Point the url at `/login/users/<username>?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "users", username]);
            url.query_pairs_mut().append_pair("session", session);

            // Send the request to delete login.
            let response = self
                .client
                .delete(url)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what has been sent back.
            match status {
                // Success
                StatusCode::NO_CONTENT => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Gets a contact from the server.
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - the resource cannot
    pub async fn get_login_uid(
        &mut self,
        username: &str,
    ) -> Result<Option<String>, Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point at
            // /login/users/<username>/uid?<session>
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["login", "users", username, "uid"]);
            url.query_pairs_mut().append_pair("session", session);

            let response = self
                .client
                .get(url)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();
            match status {
                StatusCode::OK => {
                    Ok(Some(response.text().await.map_err(Error::Networking)?))
                }
                StatusCode::NO_CONTENT => Ok(None),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }
}
