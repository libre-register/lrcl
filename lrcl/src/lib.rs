#![warn(clippy::pedantic)]
#![allow(clippy::doc_markdown)]

#[macro_use]
extern crate common_macros;
#[macro_use]
extern crate serde;
extern crate reqwest;
extern crate url;
use std::time::Duration;
pub mod contacts;
pub mod error;
pub mod login;
pub use error::Error;

/// This is a client struct, which stores a session id and a reqwest client
/// so that requests can be sent over the network.
#[derive(Debug, Clone)]
pub struct Client {
    pub client: reqwest::Client,
    pub url: reqwest::Url,
    pub session: Option<String>,
    pub server_info: lr_commons::server_info::LibreRegisterServer,
}

impl Client {
    /// Creates a new client.
    ///
    /// # Errors
    ///
    /// * [`Networking`](Error::Networking) - these are handled by reqwest.
    /// * [`UrlCannotBeABase`](Error::UrlCannotBeABase) - the url cannot be
    ///   base.
    /// * [`Url`](Error::Url) - url errors as defined by the `url` crate.
    /// * [`NotALibreRegisterServer`](Error::NotALibreRegisterServer) - the
    ///   server
    ///  is likely not a LibreRegiser server as the index is wrong.
    pub async fn new(url: &str) -> Result<Self, Error> {
        // Gets a client
        let client = reqwest::Client::builder()
            .connect_timeout(Duration::from_secs(10))
            .build()
            .map_err(Error::Networking)?;

        // Likwise get the url.
        let mut url: reqwest::Url = url.parse().map_err(Error::Url)?;

        // If the url doesn't have a port set, set it to the default of 20307
        if url.port().is_none() {
            url.set_port(Some(20307))
                .map_err(|_| Error::UrlCannotBeABase)?;
        }

        // Make a reqwest to index, to try and get the server version.
        let version = client.get(url.clone()).send();

        Ok(Self {
            client,
            url,
            session: None,
            server_info: version
                .await
                .map_err(Error::Networking)?
                .text()
                .await
                .map_err(Error::Networking)?
                .parse()
                .map_err(|_| Error::NotALibreRegisterServer)?,
        })
    }
}
