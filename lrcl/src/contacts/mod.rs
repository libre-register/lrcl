//! # Contacts
//!
//! This module has functions do with contacts.
use std::convert::TryInto;

use contack::{read_write::vcard::VCard, Contact};
use reqwest::StatusCode;

use crate::error::Error;

#[derive(Deserialize)]
pub struct ListStruct {
    pub uid: String,
    pub name: Option<String>,
}

impl super::Client {
    /// Attempts to create a contact.
    ///
    /// One should note that if the user has inadequate mutable permissions
    /// for some of the contact's fields then a half made contact may be
    /// created on the server as rather than checking that the user has
    /// access to everything, it just writes what it can.
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    ///  * [`ResourceAlreadyCreated`](Error::ResourceAlreadyCreated) - the
    ///    resource has already been created, and we shall not duplicate it.
    ///  * [`InvalidData`](Error::InvalidData) - the contact cannot be
    ///  properly dealt with on the server. To be honest its probably a
    ///  server problem.
    pub async fn create_contact(&self, vcard: &VCard) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Point the url at `/contacts?<session>`
            let mut url = self.url.join("contacts").map_err(Error::Url)?;
            url.set_query(Some(&format!("session={}", session)));

            // Send the request to create the contact.
            let response = self
                .client
                .post(url)
                .body(vcard.to_string())
                .header(
                    reqwest::header::CONTENT_TYPE,
                    reqwest::header::HeaderValue::from_static("text/vcard"),
                )
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what has been sent back.
            match status {
                // Success
                StatusCode::CREATED => Ok(()),
                StatusCode::CONFLICT => Err(Error::ResourceAlreadyCreated),
                StatusCode::BAD_REQUEST => Err(Error::InvalidData),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Generates a list of contacts from the server.
    ///
    /// # Errors
    ///
    ///  * [`UrlCannotBeABase`](Error::UrlCannotBeABase) - the url cannot be
    ///  a base.
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    error.
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - we
    ///    don't
    ///  have permissions to complete the required action.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ContackVCardParseError`](Error::ContackVCardParseError) - The
    ///    response
    ///  failed to be parsed.
    ///  * [`Contack::ContackVCardConvertError`](Error::
    ///    ContackVCardConvertError) -
    ///  converting from the VCard to the Contact failed.
    pub async fn get_contact(&mut self, uid: &str) -> Result<Contact, Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point at /contacts/list
            let mut url: reqwest::Url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["contacts", uid]);
            url.query_pairs_mut().append_pair("session", session);

            // Send the request to list the contacts
            let response = self
                .client
                .get(url)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();
            match status {
                StatusCode::OK => {
                    let text =
                        response.text().await.map_err(Error::Networking)?;
                    let contact: VCard =
                        text.parse().map_err(Error::ContackVCardParseError)?;
                    let contact: Contact = contact
                        .try_into()
                        .map_err(Error::ContackVCardConvertError)?;
                    Ok(contact)
                }
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Gets a contact from the server.
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`SerdeJson`](Error::SerdeJson) - the response failed to be parsed.
    pub async fn list_contacts(
        &mut self,
        range: std::ops::Range<usize>,
        sort_asc: bool,
        include_name: bool,
    ) -> Result<Vec<ListStruct>, Error> {
        if let Some(session) = &self.session {
            // Modifies the url to point at /contacts/list
            let mut url: reqwest::Url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["contacts", "list"]);

            let params = [
                ("session", session),
                ("sort_asc", &sort_asc.to_string()),
                ("include_name", &include_name.to_string()),
                ("offset", &range.start.to_string()),
                ("limit", &(range.end - range.start).to_string()),
            ];

            // Send the request to list the contacts
            let response = self
                .client
                .get(url)
                .form(&params)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();
            match status {
                StatusCode::OK => {
                    let text =
                        response.text().await.map_err(Error::Networking)?;
                    serde_json::from_str(&text).map_err(Error::SerdeJson)
                }
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Attempts to update a contact.
    ///
    /// One should note that if the user has inadequate mutable permissions
    /// for some of the contact's fields then a half updated contact may be
    /// created on the server as rather than checking that the user has
    /// access to everything, it just writes what it can. You should check
    /// permissions beforehand.
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - the resource cannot
    ///    be found.
    ///  * [`InvalidData`](Error::InvalidData) - the contact cannot be
    ///  properly dealt with on the server. To be honest its probably a
    ///  server problem.
    pub async fn update_contact(
        &self,
        vcard: &VCard,
        uid: &str,
    ) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Point the url at `/contacts/<uid>?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["contacts", uid]);
            url.query_pairs_mut().append_pair("session", session);

            // Send the request to create the contact.
            let response = self
                .client
                .patch(url)
                .body(vcard.to_string())
                .header(
                    reqwest::header::CONTENT_TYPE,
                    reqwest::header::HeaderValue::from_static("text/vcard"),
                )
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what has been sent back.
            match status {
                // Success
                StatusCode::NO_CONTENT => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::BAD_REQUEST => Err(Error::InvalidData),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }

    /// Deletes a contact.
    ///
    /// You need the following permissions:
    ///
    ///  * `/contacts/UID`
    ///  * `/contacts/N`
    ///      * `/contacts/N/given`
    ///      * `/contacts/N/family`
    ///      * `/contacts/N/additional`
    ///      * `/contacts/N/prefixes`
    ///      * `/contacts/N/suffixes`
    ///  * `/contacts/NICKNAME`
    ///  * `/contacts/ANNIVERSARY`
    ///  * `/contacts/BDAY`
    ///  * `/contacts/PHOTO`
    ///  * `/contacts/TITLE`
    ///  * `/contacts/ROLE`
    ///  * `/contacts/ORG`
    ///      * `/contacts/ORG/org`
    ///      * `/contacts/ORG/unit`
    ///      * `/contacts/ORG/office`
    ///  * `/contacts/LOGO`
    ///  * `/contact/CI`
    ///  * `/contacts/ADR`
    ///      * `/contacts/ADR/work`
    ///      * `/contacts/ADR/home`
    ///
    /// # Errors
    ///  * [`Networking`](Error::Networking) - reqwest has had a networking
    ///    errors
    ///  * [`Url`](Error::Url) - issues with the url.
    ///  * [`LoginInvalidSession`](Error::LoginInvalidSession) - the client (us)
    ///    thinks we're logged in, but the server doesn't.
    ///  * [`LoginInvalidPermissions`](Error::LoginInvalidPermissions) - the
    ///    client (us) thinks we're logged in, but the server doesn't.
    ///  * [`InvalidStatusCodeInResponse`](Error::InvalidStatusCodeInResponse) -
    ///    the status code given has sincerely confused us.
    ///  * [`NotLoggedIn`](Error::NotLoggedIn) - the client is not logged in.
    ///  * [`ResourceNotFound`](Error::ResourceNotFound) - the resource cannot
    ///    be found.
    pub async fn delete_contact(&self, uid: &str) -> Result<(), Error> {
        if let Some(session) = &self.session {
            // Point the url at `/contacts/<uid>?<session>`
            let mut url = self.url.clone();
            url.path_segments_mut()
                .map_err(|_| Error::UrlCannotBeABase)?
                .extend(&["contacts", uid]);
            url.query_pairs_mut().append_pair("session", session);

            // Send the request to delete the contact.
            let response = self
                .client
                .delete(url)
                .send()
                .await
                .map_err(Error::Networking)?;

            let status = response.status();

            // See what has been sent back.
            match status {
                // Success
                StatusCode::NO_CONTENT => Ok(()),
                StatusCode::NOT_FOUND => Err(Error::ResourceNotFound),
                StatusCode::UNAUTHORIZED => match response
                    .text()
                    .await
                    .map_err(Error::Networking)?
                    .as_str()
                {
                    "INVALID_PERMISSIONS" => {
                        Err(Error::LoginInvalidPermissions)
                    }
                    "INVALID_SESSION" => Err(Error::LoginInvalidSession),
                    _ => {
                        Err(Error::InvalidStatusCodeInResponse(status.as_u16()))
                    }
                },
                _ => Err(Error::InvalidStatusCodeInResponse(
                    response.status().as_u16(),
                )),
            }
        } else {
            Err(Error::NotLoggedIn)
        }
    }
}
