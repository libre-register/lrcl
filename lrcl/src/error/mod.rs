use std::error::Error as StdError;
use std::fmt;

/// All the errors which can occur
#[derive(Debug)]
pub enum Error {
    /// Used to indicate that the server isn't a LibreRegiser server,
    /// used when first connecting to the server. Moreover, if the
    /// response does not make any sense, as in it is not in the correct
    /// format for the index function, than this error is returned.
    NotALibreRegisterServer,
    /// This indicates an error to do with parsing the Url, as defined
    /// by the `url` crate.
    Url(url::ParseError),
    /// The Url cannot be base. I don't know why it just can't.
    UrlCannotBeABase,
    /// A networking error as defined by reqwest, this may include things
    /// such as timeouts or suchlike.
    Networking(reqwest::Error),
    /// An invalid session. This is used both when trying to log in and
    /// failing to do so, and when trying to preform an action with an
    /// expired session.
    LoginInvalidSession,
    /// Indicates that the action cannot be completed because the user does
    /// not have the required permissions.
    LoginInvalidPermissions,
    /// Indicates a user needs to be logged in, but isn't.
    NotLoggedIn,
    /// Indicates the server returned with a status code which cannot be
    /// understood in the current context.
    InvalidStatusCodeInResponse(u16),
    /// The resource which we attempted to create has already been created.
    ResourceAlreadyCreated,
    /// The resource which we attempted to modify does not exist.
    ResourceNotFound,
    // This is returned when the data given into the function is in some way
    // invalid.
    InvalidData,
    /// Indicates that serde json failed to parse some json sent by the
    /// server or to write some json to the server
    SerdeJson(serde_json::Error),
    /// Indicates that parsing from a vcard has failed.
    ContackVCardParseError(
        contack::read_write::component::error::ComponentParseError,
    ),
    /// Indicates that when converting from a vcard to a usable datatype -
    /// [Contact](contack::Contact) - something went wrong.
    ContackVCardConvertError(contack::read_write::error::FromComponentError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::NotALibreRegisterServer => {
                write!(f, "Not a (valid) Libre Register server")
            }
            Self::Url(e) => write!(f, "Url Error: {}", e),
            Self::UrlCannotBeABase => write!(f, "Url cannot be a base."),
            Self::Networking(e) => write!(f, "Networking Error: {}", e),
            Self::LoginInvalidSession => {
                write!(
                    f,
                    "Invalid login session; or invalid username or password."
                )
            }
            Self::LoginInvalidPermissions => {
                write!(f, "You don't have the required permissions.")
            }
            Self::NotLoggedIn => write!(f, "Not logged in"),
            Self::InvalidStatusCodeInResponse(e) => write!(
                f,
                "An invalid HTTP status code was sent in the response: {}",
                e
            ),
            Self::ResourceAlreadyCreated => {
                write!(f, "Resource already created")
            }
            Self::ResourceNotFound => write!(f, "Resource not found"),
            Self::InvalidData => write!(f, "Invalid data"),
            Self::SerdeJson(e) => write!(f, "Json failed: {}", e),
            Self::ContackVCardParseError(e) => {
                write!(f, "Parsing from VCard failed: {}", e)
            }
            Self::ContackVCardConvertError(e) => {
                write!(f, "Converting from a parsed VCard failed: {}", e)
            }
        }
    }
}

impl StdError for Error {}
